package com.truedmp.examplecallbackapp;

public interface OnConfirmDeleteListener {

    void onDelete();
    void onCancel();

}
