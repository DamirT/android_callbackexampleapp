package com.truedmp.examplecallbackapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class OptionsDialog extends DialogFragment {

    private onSelectListener onSelectListener;

    public void setOnSelectListener(onSelectListener onSelectListener) {
        this.onSelectListener = onSelectListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TIKI", "onCreate: First this is trigered");
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("TIKI", "onCreateView: Second this is trigered");
        View view = inflater.inflate(R.layout.dialog_options, container, false);
        Button btnEdit = view.findViewById(R.id.dialog_options_btn_edit);
        Button btnDelete = view.findViewById(R.id.dialog_options_btn_delete);
        Button btnCancel = view.findViewById(R.id.dialog_options_btn_cancel);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //callback here
                if (onSelectListener != null) {
                    onSelectListener.onEdit();
                }
                dismiss();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConfirmDeleteDialog confirmDeleteDialog = new ConfirmDeleteDialog();
                confirmDeleteDialog.show(getFragmentManager(), ConfirmDeleteDialog.class.getSimpleName());
                confirmDeleteDialog.setConfirmDeleteDialog(new OnConfirmDeleteListener() {
                    @Override
                    public void onDelete() {
                        if (onSelectListener != null) {
                            onSelectListener.onDelete();
                        }
                        dismiss();
                    }

                    @Override
                    public void onCancel() {
                        if (onSelectListener != null) {
                            onSelectListener.onCancel();
                        }
                        dismiss();
                    }
                });
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onSelectListener != null) {
                    onSelectListener.onCancel();
                }
                dismiss();
            }
        });

        //handle click events
        return view;
    }
}
