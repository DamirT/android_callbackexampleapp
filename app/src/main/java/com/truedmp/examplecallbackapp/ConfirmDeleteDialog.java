package com.truedmp.examplecallbackapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class ConfirmDeleteDialog extends DialogFragment {

    OnConfirmDeleteListener onConfirmDeleteListener;

    public void setConfirmDeleteDialog(OnConfirmDeleteListener onConfirmDeleteListener) {
        this.onConfirmDeleteListener = onConfirmDeleteListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_confirm_delete, container, false);
        Button btnConfirmDelete = view.findViewById(R.id.dialog_confirm_btn_delete_yes);
        Button btnConfirmDeleteCancel = view.findViewById(R.id.dialog_confirm_delete_btn_cancel);

        btnConfirmDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onConfirmDeleteListener != null) {
                    onConfirmDeleteListener.onDelete();
                }
                dismiss();
            }
        });
        btnConfirmDeleteCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onConfirmDeleteListener != null) {
                    onConfirmDeleteListener.onCancel();
                }
                dismiss();
            }
        });

        return view;
    }
}
