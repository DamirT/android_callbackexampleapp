package com.truedmp.examplecallbackapp;

public interface onSelectListener {

    void onEdit();
    void onDelete();
    void onCancel();
}
